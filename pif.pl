#! /usr/bin/perl 

use strict;

use 5.010;
use File::Find;
use File::Basename;
use Getopt::Long;
use Data::Dumper;

my $help;
my $backup;

my @cheaders = qw/assert.h complex.h ctype.h errno.h fenv.h float.h inttypes.h 
iso646.h limits.h locale.h math.h setjmp.h signal.h stdalign.h stdarg.h stdbool.h
stddef.h  stdint.h stdio.h stdlib.h string.h tgmath.h time.h uchar.h wchar.h
wctype.h
cassert ccomplex cctype cerrno cfenv cfloat cinittypes ciso646 climits clocale 
cmath csetjmp csignal cstdalign cstdarg cstdbool cstdded cstdint cstdio cstdlib 
cstring ctgmath ctime cuchar cwchar cwctype/;

my @cppheaders = qw/array bitset deque forward_list list map queue set stack
 
unordered_map unordered_set vector ios istream iostream fstream sstream atomic 
condition_variable future mutex thread algorithm chrono codecvt complex exception 
functional initializer_list iterator limits locale memory new numeric random ratio 
regex stdexcept string system_error tuple typeindex typeinfo type_traits utility valarray /;

my %custom_group;
my %old_custom_group;

GetOptions("h|help|?" => \$help,"b|backup" => \$backup, "g|group=s" =>\%custom_group);

&say_help() if $help;
if ($#ARGV ==-1)
{
	@ARGV = ('.');
} 
find({wanted => \&process, preprocess =>\&preprocess, postprocess =>\&postprocess},@ARGV);

################################################

sub preprocess
{
	my $key;
	my $val;
	open FH, "<.pif" or return @_;
	%old_custom_group=%custom_group;
	%custom_group=();
	while(<FH>)
	{
		chomp;
		($key,$val)=split "=";
		$custom_group{$key}=$val;
	}
	close FH;
	return @_;
}

sub postprocess
{
	%custom_group=%old_custom_group;
}

sub process
{
	if (not &check_name($_))
	{
		return;
	}
	my $if_cnt =0;
	my @inc = [];
	my @buf;
	my $myfile = $_;
	rename $_,"$_.bak";
	open IN,"$_.bak";
	open OUT, ">",$_;
	while (<IN>)
	{
		if ((/^\s*$/o) || (/^(#|\/\/)/o))
		{
			if(/#if/o)
			{
				++$if_cnt;
				$inc[$if_cnt] =[]
			}
			--$if_cnt if /#endif/o;
			push @buf,$_;
			push $inc[$if_cnt],$_ if /^#include/o;
		}
		else
		{
			last;
		}

	}
	for (my $i =0; $i <=$#inc; $i++)
	{
		$inc[$i]=&inc_sort($myfile,\%custom_group,$inc[$i]);
	}
	@buf = &change_inc(\@buf,\@inc);
	print OUT @buf;
	print OUT $_;
	print OUT <IN>;
	close IN;
	close OUT;
	unlink "$myfile.bak" if !$backup; 
}

sub check_name
{
	if ($_[0]=~/\.(c|cpp|h|hpp|cxx|hxx)$/io)
	{
		return 1;
	}
	0;
}



sub inc_sort
{
	my $name = shift;
	my $c_group = shift;
	my $arr= shift;
	$name =~ s/\.\w+//;
	my @res;
	my %sorted;
	my $own = 100; #own name
	my $cstd = 200; #cstd
	my $cppstd = 300; #cppstd
	my $lib = 400; #lib
	my $system = 500; #system
	my $local = 600; #locale
	my $c=0;
	foreach my $key (keys %$c_group)
	{
		my $i =0;
		$sorted{$key} = [];
		if($c_group->{$key}=~/^-own$/o)
		{
			$own=$key;
			next;
		}
		elsif($c_group->{$key}=~/^-cstd$/o)
		{
			$cstd=$key;
			next;
		}
		elsif($c_group->{$key}=~/^-cppstd$/o)
		{
			$cppstd=$key;
			next;
		}
		elsif($c_group->{$key}=~/^-lib$/o)
		{
			$lib=$key;
			next;
		}
		elsif($c_group->{$key}=~/^-system$/o)
		{
			$system=$key;
			next;
		}
		elsif($c_group->{$key}=~/^-local$/o)
		{
			$local=$key;
			next;
		}
		foreach my $elem (@$arr)
		{
			if ($elem =~/$c_group->{$key}/)
			{
				push @{$sorted{$key}},$elem;
				splice @$arr,$i,1;
			}
			++$i;
		}
	}
	foreach  my $elem (@$arr)
	{
		if ($elem =~/$name/)
		{
			push @{$sorted{$own}}, $elem;
			next;
		}
		elsif(grep {$elem=~/<$_>/;} @cheaders)
		{
			push @{$sorted{$cstd}}, $elem;
			next;
		}
		elsif(grep {$elem=~/<$_>/;} @cppheaders)
		{
			push @{$sorted{$cppstd}},$elem;
			next;
		}
		elsif($elem=~/<.*\//o)
		{
			push @{$sorted{$lib}},$elem;
			next;
		}
		elsif($elem=~/</o)
		{
			push @{$sorted{$system}},$elem;
			next;
		}
		else
		{
			push @{$sorted{$local}},$elem;
		}
	}
	foreach my $key (sort {$a<=>$b} keys %sorted)
	{
		push @res, sort @{$sorted{$key}};
	}
	return \@res;
}

sub change_inc
{
	my $buf=shift;
	my $inc=shift;
	my @i=(0);
	my $if_cnt=0;
	for (my $j=0;$j<=$#$buf;$j++)
	{
		if($buf->[$j]=~/#if/o)
		{
			++$if_cnt;
			$i[$if_cnt]=0;
		}
		--$if_cnt if $buf->[$j] =~ /#endif/o;
		if ($buf->[$j]=~/#include/o)
		{
			$buf->[$j]=$inc->[$if_cnt][$i[$if_cnt]];
			$i[$if_cnt]++;
		}
	}
	return @$buf;
}

sub say_help
{
	say "@{[basename($0)]}  [path]
The script sorts include directives in C++ source file
-h --help	say help
-b -- backup	make file backup
-g --group	hash value for custom headers group (groupno=pattern)";
exit 1;
}

